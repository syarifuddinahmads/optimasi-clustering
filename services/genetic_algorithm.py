from operator import itemgetter
from random import random
from numpy.random import randint
from numpy.random import rand
import random


class GeneticAlgorithm:

    def __init__(self, data, cluster=2, crossover=0, mutation=0, max_iteration=1500, population_size=100):
        self.data = data
        self._clusters = cluster
        self._crossovers = crossover
        self._mutations = mutation
        self._max_iterations = max_iteration
        self._population_size = population_size

    # dirubah ke sse
    def calculate_fitnes(self, distance):
        if distance == 0:
            return 0
        return 1 / abs(distance)

    def calculate_distance(self, gen):
        total_distance = 0
        for i in gen:
            total_distance += pow(i[0] - i[1], 2)
        return pow(total_distance, 2)

    def generate_chromosomes(self):
        chromosome = []
        while len(chromosome) < self._clusters:
            genotype = random.choice(self.data)
            if genotype not in chromosome:
                chromosome.append(genotype)

        return chromosome

    def selection(self, pop, scores, k=3):
        index = randint(len(pop))
        for idx in randint(0, len(pop), k-1):
            if scores[idx]['fitnes_average_sse'] < scores[index]['fitnes_average_sse']:
                index = idx
        return pop[index]

    def crossover(self, parent1, parent2):
        child_one, child_two = parent1.copy(), parent2.copy()
        if rand() < self._crossovers:
            pt = randint(1, len(parent1))
            child_one = parent1[:pt] + parent2[pt:]
            child_two = parent2[:pt] + parent1[pt:]
        return [child_one, child_two]

    def mutation(self, children):
        for i in range(len(children)):
            if rand() < self._mutations:
                children[i] = children[i]

    def initial_population(self):
        population = []
        for i in range(self._population_size):
            chromosome = self.generate_chromosomes()
            population.append(chromosome)
        return population

    def fitnes_population(self, population):
        new_population = []
        for i in population:
            distance = self.calculate_distance(i)
            average_distance = abs(distance/self._clusters)
            fitnes_distance_sse = self.calculate_fitnes(distance)
            fitnes_average_sse = self.calculate_fitnes(average_distance)
            data = {"chromosome": i, "distance": distance, "fitnes_distance_sse": fitnes_distance_sse,
                    "sse": fitnes_distance_sse, "average_distance": average_distance, "fitnes_average_sse": fitnes_average_sse}
            new_population.append(data)
        return new_population

    def run(self):

        population = self.initial_population()

        fit_population = sorted(self.fitnes_population(
            population), key=itemgetter('fitnes_average_sse'), reverse=True)

        best, best_eval = None, fit_population[0]["fitnes_average_sse"]

        for gen in range(self._max_iterations):

            scores = self.fitnes_population(population)

            for i in range(self._population_size):
                if scores[i]["fitnes_average_sse"] < best_eval:
                    best, best_eval = population[i], scores[i]["fitnes_average_sse"]
                    # print(">%d, new best f(%s) = %f" %
                    #       (gen,  population[i], scores[i]["sse"]))

            selected = [self.selection(population, scores)
                        for _ in range(self._population_size)]

            children = list()
            for i in range(0, self._population_size, 2):
                p1, p2 = selected[i], selected[i+1]

                for c in self.crossover(p1, p2):

                    self.mutation(c)

                    children.append(c)

            population = children

        return best, best_eval
