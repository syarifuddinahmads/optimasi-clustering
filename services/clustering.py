from csv import DictWriter, writer
from operator import le
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.preprocessing import MinMaxScaler
from services.genetic_algorithm import GeneticAlgorithm
from services.helper import Helper
from sklearn.cluster import KMeans
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
import seaborn as sns
from matplotlib import pyplot as plt

from services.kmeans import K_Means


class Clustering:

    def __init__(self, data, cluster=2, crossover=0, mutation=0, max_iteration=1500, population_size=100, start_date='', end_date=''):
        self.data = data
        self._clusters = cluster
        self._crossovers = crossover
        self._mutations = mutation
        self._max_iterations = max_iteration
        self._population_size = population_size
        self._start_date = start_date
        self._end_date = end_date
        self._dir_cluster = "static/cluster/"
        self._dir_elbow = "static/elbow/"

    def process(self):

        _letter = Helper(length=10).randomword()
        print("Letter : ", _letter)

        # sse, cluster_fig, elbow_fig = self.kmeans(letter=_letter)
        # sse_ga, cluster_fig_ga, elbow_fig_ga, sse = self.ga_kmeans(
        #     letter=_letter)

        print("=================== KMEANS ========================")
        sse_kmeans, figure_kmeans = self._kmeans(letter=_letter)
        print("=================== KMEANS ========================")
        print("=================== GA-KMEANS ========================")
        sse_ga_kmeans, figure_ga_kmeans = self._ga_kmeans(letter=_letter)
        print("=================== GA-KMEANS ========================")

        field_names = ['total_cluster', 'total_data', 'total_populasi', 'max_iterations', 'crossover_rate', 'mutation_rate', 'sse_ga','_sse_ga', 'sse_kmeans', '_sse_kmeans',
                       'data_kmeans', 'data_ga', 'start_date', 'end_date', 'thumbnail_cluster_kmeans', 'thumbnail_cluster_ga']
        data_clustering = {'total_cluster': self._clusters, 'total_data': len(self.data), 'total_populasi': self._population_size, 'sse_ga': sse_ga_kmeans,'_sse_ga': 1/sse_ga_kmeans, 'sse_kmeans': sse_kmeans, '_sse_kmeans': 1/sse_kmeans, 'crossover_rate': self._crossovers, 'mutation_rate': self._mutations, 'data_kmeans': '-',
                           'data_ga': '-', 'max_iterations': self._max_iterations, 'start_date': self._start_date, 'end_date': self._end_date, 'thumbnail_cluster_kmeans': 'cluster/'+figure_kmeans, 'thumbnail_cluster_ga': 'cluster/'+figure_ga_kmeans}
        print("Result : ", data_clustering)
        with open('./data/clustering.csv', 'a+', newline='\n') as f_object:

            dictwriter_object = DictWriter(
                f_object, fieldnames=field_names)
            dictwriter_object.writerow(data_clustering)
            f_object.close()

    def ga_kmeans(self, letter):

        name_file_cluster_ga_kmeans = letter+"_ga_kmeans_cluster.png"
        name_file_elbow_ga_kmeans = letter+"_ga_kmeans_elbow.png"

        scaler = MinMaxScaler()
        scale = scaler.fit_transform(self.data[['latitude', 'longitude']])
        data_scale = pd.DataFrame(scale, columns=['latitude', 'longitude'])
        K = range(1, 12)
        ga = GeneticAlgorithm(data=data_scale, cluster=self._clusters, crossover=self._crossovers,
                              mutation=self._mutations, population_size=self._population_size, max_iteration=self._max_iterations)
        best, score = ga.run()

        print("Centroid : ", best, " Score : ", score)

        init = np.array(best)
        print("N Init : ", init, " Len : ", len(init))

        ga_km = KMeans(n_clusters=self._clusters, init=best)
        ga_km.fit_predict(self.data[['latitude', 'longitude']])

        print("GA KMeans  >>> Inertia :", ga_km.inertia_, " SSE : ",
              (1/ga_km.inertia_), " Inter : ", ga_km.n_iter_)

        self.data['Clusters'] = ga_km.labels_
        sns.scatterplot(x="latitude", y="longitude",
                        hue='Clusters',  data=self.data, palette='viridis')
        plt.scatter(ga_km.cluster_centers_[:, 0], ga_km.cluster_centers_[
            :, 1], c='red', s=50)
        plt.savefig(self._dir_cluster +
                    name_file_cluster_ga_kmeans, dpi=500)
        plt.cla()

        ga_wss = []
        for k in K:
            kmeans = KMeans(n_clusters=k)
            kmeans = kmeans.fit(data_scale)
            ga_wss.append(kmeans.inertia_)

        plt.xlabel('K')
        plt.ylabel('Sum of Squared Errors (SSE)')
        plt.plot(K, ga_wss, 'bx-')
        plt.grid(True)
        plt.savefig(self._dir_elbow+name_file_elbow_ga_kmeans, dpi=500)
        plt.cla()

        return ga_km.inertia_, name_file_cluster_ga_kmeans, name_file_elbow_ga_kmeans, score

    def kmeans(self, letter):

        name_file_cluster_kmeans = letter+"_kmeans_cluster.png"
        name_file_elbow_kmeans = letter+"_kmeans_elbow.png"

        scaler = MinMaxScaler()
        scale = scaler.fit_transform(self.data[['latitude', 'longitude']])
        data_scale = pd.DataFrame(scale, columns=['latitude', 'longitude'])
        K = range(1, 12)

        km = KMeans(n_clusters=self._clusters)
        km.fit_predict(self.data[['latitude', 'longitude']])
        print("KMeans  >>> Inertia :",
              (1/km.inertia_), " Inter : ", km.inertia_, " SSE : ", km.n_iter_)

        self.data['Clusters'] = km.labels_
        sns.scatterplot(x="latitude", y="longitude",
                        hue='Clusters',  data=self.data, palette='viridis')
        plt.scatter(km.cluster_centers_[:, 0], km.cluster_centers_[
            :, 1], c='red', s=50)
        plt.savefig(self._dir_cluster+name_file_cluster_kmeans, dpi=500)
        plt.cla()

        wss = []
        for k in K:
            kmeans = KMeans(n_clusters=k)
            kmeans = kmeans.fit(data_scale)
            wss.append(kmeans.inertia_)

        plt.xlabel('K')
        plt.ylabel('Sum of Squared Errors (SSE)')
        plt.plot(K, wss, 'bx-')
        plt.grid(True)
        plt.savefig(self._dir_elbow+name_file_elbow_kmeans, dpi=500)
        plt.cla()

        return km.inertia_, name_file_cluster_kmeans, name_file_elbow_kmeans

    def _kmeans(self, letter):

        name_file_cluster_kmeans = letter+"_kmeans_cluster.png"

        colors = np.random.rand(self._clusters, 3)

        _data_transform = self.tranform_data(
            self.data, "latitude", "longitude")

        k_means = K_Means(data=_data_transform, cluster=self._clusters,
                          max_iter=self._max_iterations)
        k_means.run()

        kmeans_result = {'clusters': '',
                         'centroids': k_means.centroids, 'sse': k_means.sse, 'best_sse': sum(k_means.sse)}

        # print("KMeans From Scratch : ", kmeans_result)

        for classification in k_means.classifications:
            for featureset in k_means.classifications[classification]:
                plt.scatter(featureset[0], featureset[1],
                            marker="x", color=colors[classification], s=100, alpha=1)

        for centroid in range(self._clusters):
            plt.scatter(k_means.centroids[centroid][0], k_means.centroids[centroid][1],
                        marker="o", color='r', s=100, alpha=1)

        plt.xlabel('Latitude')
        plt.ylabel('Longitude')
        plt.savefig(self._dir_cluster+name_file_cluster_kmeans, dpi=500)
        plt.cla()

        return sum(k_means.sse), name_file_cluster_kmeans

    def _ga_kmeans(self, letter):

        name_file_cluster_ga_kmeans = letter+"_ga_kmeans_cluster.png"
        colors = np.random.rand(self._clusters, 3)

        _data_transform = self.tranform_data(
            self.data, "latitude", "longitude")

        ga = GeneticAlgorithm(data=_data_transform, cluster=self._clusters, crossover=self._crossovers,
                              mutation=self._mutations, population_size=self._population_size, max_iteration=self._max_iterations)
        _init_centroid, score = ga.run()

        print("Centroid : ", _init_centroid, " Score : ", score)

        k_means = K_Means(data=_data_transform, cluster=self._clusters,
                          max_iter=self._max_iterations, init_centroid=_init_centroid)
        k_means.run()

        kmeans_result = {'clusters': '',
                         'centroids': k_means.centroids, 'sse': k_means.sse, 'best_sse': sum(k_means.sse)}

        # print("GA KMeans From Scratch : ", kmeans_result)

        for classification in k_means.classifications:
            for featureset in k_means.classifications[classification]:
                plt.scatter(featureset[0], featureset[1],
                            marker="x", color=colors[classification], s=100, alpha=1)

        for centroid in range(self._clusters):
            plt.scatter(k_means.centroids[centroid][0], k_means.centroids[centroid][1],
                        marker="o", color='r', s=100, alpha=1)

        plt.xlabel('Latitude')
        plt.ylabel('Longitude')
        plt.savefig(self._dir_cluster+name_file_cluster_ga_kmeans, dpi=500)
        plt.cla()

        return sum(k_means.sse), name_file_cluster_ga_kmeans

    def tranform_data(self, data, x_column_name, y_column_name):
        data_transform = []
        print("Data Type : ", type(data))
        latitude = data[x_column_name].tolist()
        longitude = data[y_column_name].tolist()
        for i in range(len(data)):
            xy = [latitude[i], longitude[i]]
            for i in range(len(xy)):
                xy[i] = float(xy[i])
            data_transform.append(xy)
        return data_transform
