import random, string


class Helper:

    def __init__(self,length = 10):
        self.length = length

    def tranform_data(self,data,x_column_name,y_column_name):
        data_transform = []
        for _data in data:
            xy = [_data[x_column_name],_data[y_column_name]] 
            for i in range(len(xy)):
                xy[i] = float(xy[i])
            data_transform.append(xy)
        return data_transform

    def randomword(self):
        letters = '0123456789abcdefghijklmnopqrstuvwxyz'
        return ''.join(random.choice(letters) for i in range(self.length))
