from math import sqrt
from random import randint
import random
import numpy as np


class K_Means:

    def __init__(self, data, cluster=2, tolerance=0.001, max_iter=1500, init_centroid=[]):
        print("Len Centroid : ", init_centroid)
        self.clusters = cluster
        self.max_iterations = max_iter
        self.tolerance = tolerance
        self.data = data
        self.classifications = {}
        self.sse = []
        self.centroids = {}
        self.init_centroid = init_centroid

    def predict(self):
        distances = [np.linalg.norm(self.data-self.centroids[centroid])
                     for centroid in self.centroids]
        classification = distances.index(min(distances))
        return classification

    def euclidean_distance(self, point1, point2):
        return np.linalg.norm(np.array(point1)-np.array(point2), axis=0)

    def computeSSE(self):
        temp_centroid = {}
        for i in range(self.clusters):
            centroid = self.centroids[i]
            temp_se = []
            for point in self.data:
                temp_se.append(self.euclidean_distance(
                    point1=point, point2=centroid))
            temp_centroid[i] = temp_se

        for i in range(len(self.data)):
            temp_sse = []
            for j in temp_centroid:
                temp_sse.append(temp_centroid[j][i])
            self.sse.append(min(temp_sse))

    def run(self):

        # random dari kmeans sendiri
        if len(self.init_centroid) == 0:
            # random selection data
            temp_centroid = random.choices(self.data, k=self.clusters)
            for i in range(self.clusters):
                self.centroids[i] = temp_centroid[i]
        else:
            # init dari GA
            for i in range(self.clusters):
                self.centroids[i] = self.init_centroid[i]

        print("Centroid Init >>> : ", self.centroids)

        for i in range(self.max_iterations):

            print("Centroid 1 ::: ", self.centroids)

            for j in range(self.clusters):
                self.classifications[j] = []

            for point in self.data:
                distances = []
                for index in self.centroids:
                    distances.append(self.euclidean_distance(
                        point, self.centroids[index]))
                cluster_index = distances.index(min(distances))
                self.classifications[cluster_index].append(point)

            print("Centroid 2 ::: ", self.centroids)

            previous = dict(self.centroids)

            for cidx in self.classifications:
                self.centroids[cidx] = np.average(
                    self.classifications[cidx], axis=0)

            print("Centroid 3 ::: ", self.centroids)
            print("Centroid Prev ::: ", previous)

            isOptimal = True
            for centroid in self.centroids:
                original_centroid = previous[centroid]
                curr = self.centroids[centroid]
                print("Centroid : ", centroid, " Original : ",
                      original_centroid, " Current : ", curr)
                if np.sum((curr-original_centroid)/original_centroid*100.0) > self.tolerance:
                    isOptimal = False
            if isOptimal:
                break
        self.computeSSE()
