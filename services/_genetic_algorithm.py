from numpy.random import randint
from numpy.random import rand


class GeneticAlgorithm:

    def __init__(self, data, bounds, cluster=2, crossover=0, mutation=0, max_iteration=1500, population_size=2, n_bit=16):
        self.data = data
        self._bounds = bounds
        self._clusters = cluster
        self._crossovers = crossover
        self._mutations = mutation
        self._max_iterations = max_iteration
        self._population_size = population_size
        self._bits = n_bit

    # objective function
    def objective(self,x):
        return x[0]**2.0 + x[1]**2.0

    # decode bitstring to numbers
    def decode(self, bitstring):
        decoded = list()
        largest = 2**self._bits
        for i in range(len(self._bounds)):
            # extract the substring
            start, end = i * self._bits, (i * self._bits)+self._bits
            substring = bitstring[start:end]
            # convert bitstring to a string of chars
            chars = ''.join([str(s) for s in substring])
            # convert string to integer
            integer = int(chars, 2)
            # scale integer to desired range
            value = self._bounds[i][0] + (integer/largest) * \
                (self._bounds[i][1] - self._bounds[i][0])
            # store
            decoded.append(value)
        return decoded

    # tournament selection
    def selection(self, pop, scores, k=3):
        # first random selection
        selection_ix = randint(len(pop))
        for ix in randint(0, len(pop), k-1):
            # check if better (e.g. perform a tournament)
            if scores[ix] < scores[selection_ix]:
                selection_ix = ix
        return pop[selection_ix]

    # crossover two parents to create two children
    def crossover(self, p1, p2):
        # children are copies of parents by default
        c1, c2 = p1.copy(), p2.copy()
        # check for recombination
        if rand() < self._crossovers:
            # select crossover point that is not on the end of the string
            pt = randint(1, len(p1)-2)
            # perform crossover
            c1 = p1[:pt] + p2[pt:]
            c2 = p2[:pt] + p1[pt:]
        return [c1, c2]

    # mutation operator
    def mutation(self, bitstring, r_mut):
        for i in range(len(bitstring)):
            # check for a mutation
            if rand() < r_mut:
                # flip the bit
                bitstring[i] = 1 - bitstring[i]

    # genetic algorithm
    def run(self):
        print("Bounds ", self._bounds)
        # initial population of random bitstring
        pop = [randint(0, 1, self._bits*len(self._bounds)).tolist() for _ in range(self._population_size)]
        print("Population ",len(pop))
        # keep track of best solution
        best, best_eval = 0, self.objective(self.decode(pop[0]))
        # enumerate generations
        for gen in range(self._max_iterations):
            # decode population
            decoded = [self.decode(p) for p in pop]
            # evaluate all candidates in the population
            scores = [self.objective(d) for d in decoded]
            # check for new best solution
            for i in range(self._population_size):
                if scores[i] < best_eval:
                    best, best_eval = pop[i], scores[i]
                    print("> %d, new best f(%s) = %f" %
                          (gen,  decoded[i], scores[i]))
            # select parents
            selected = [self.selection(pop, scores) for _ in range(self._population_size)]
            # create the next generation
            children = list()
            for i in range(0, self._population_size, 2):
                # get selected parents in pairs
                p1, p2 = selected[i], selected[i+1]
                # crossover and mutation
                for c in self.crossover(p1, p2):
                    # mutation
                    self.mutation(c, self._mutations)
                    # store for next generation
                    children.append(c)
            # replace population
            pop = children
            return [best, best_eval]
