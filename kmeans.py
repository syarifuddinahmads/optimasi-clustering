# load library
from sklearn.cluster import KMeans
from sklearn import preprocessing
import sklearn.cluster as cluster
import sklearn.metrics as metrics
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
import seaborn as sns
from matplotlib import pyplot as plt

# load data
# df = pd.read_csv('./data/sebaran-hotspot-agustus.csv')
# df = pd.read_csv('./data/sebaran-hotspot.csv')
df = pd.read_csv('./data/sebaran-hotspot-4.csv')
df.head()

df.shape

scaler = MinMaxScaler()
scale = scaler.fit_transform(df[['latitude','longitude']])
df_scale = pd.DataFrame(scale, columns = ['latitude','longitude']);
df_scale.head(5)

km=KMeans(n_clusters=7)
y_predicted = km.fit_predict(df[['latitude','longitude']])
y_predicted

km.cluster_centers_

df['Clusters'] = km.labels_
sns.scatterplot(x="latitude", y="longitude",hue = 'Clusters',  data=df,palette='viridis')
plt.savefig("test_cluster.png", dpi=500)
plt.cla()

K=range(1,12)
wss = []

for k in K:
    kmeans=cluster.KMeans(n_clusters=k)
    kmeans=kmeans.fit(df_scale)
    wss_iter = kmeans.inertia_
    wss.append(wss_iter)

plt.xlabel('K')
plt.ylabel('Within-Cluster-Sum of Squared Errors (WSS)')
plt.plot(K,wss)
plt.savefig("test_elbow.png", dpi=500)
plt.cla()


for i in range(2,13):
    labels=cluster.KMeans(n_clusters=i,random_state=200).fit(df_scale).labels_
    print ("Silhouette score for k(clusters) = "+str(i)+" is "
    +str(metrics.silhouette_score(df_scale,labels,metric="euclidean",sample_size=1000,random_state=200)))