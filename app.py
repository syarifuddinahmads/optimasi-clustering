from flask import Flask

app = Flask(__name__)
app.secret_key = 'optimasi-clustering'

# import controllers
from controllers.home import home_ctrl
from controllers.data import data_ctrl
from controllers.clustering import clustering_ctrl

# register blueprints
app.register_blueprint(home_ctrl)
app.register_blueprint(data_ctrl)
app.register_blueprint(clustering_ctrl)

# run app with port 5000
if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
