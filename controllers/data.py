from flask import Blueprint, render_template
import pandas as pd

# define blueprint
data_ctrl = Blueprint('data_controller', __name__)
title_page = "Data"


@data_ctrl.route('/data')
def index():
    hotspots = pd.read_csv('./data/sebaran-hotspot-agustus.csv')
    data = {
        "title_page": title_page,
        "hotspots": list(hotspots.values)
    }
    return render_template('data/index.htm', datas=data)
