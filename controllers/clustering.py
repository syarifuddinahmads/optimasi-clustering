from datetime import datetime
from flask import Blueprint, redirect, render_template, request
import pandas as pd
from sklearn.cluster import KMeans
import pandas as pd

from services.clustering import Clustering
clustering_ctrl = Blueprint('clustering_controller', __name__)
title_page = "Clustering"


@clustering_ctrl.route('/clustering')
def index():
    clustering = pd.read_csv('./data/clustering.csv')
    data = {
        "title_page": title_page,
        "clusterings": list(clustering.values)
    }
    return render_template('clustering/index.htm', datas=data)


@clustering_ctrl.route('/clustering/create')
def create():
    mutations = pd.read_csv('./data/mutation-rates.csv')
    crossovers = pd.read_csv('./data/crossover-rates.csv')
    data = {
        "title_page": title_page,
        "crossovers": list(crossovers.values),
        "mutations": list(mutations.values)
    }
    return render_template('clustering/create.htm', datas=data)


@clustering_ctrl.route('/clustering/detail/<int:id>')
def show(id):
    clustering = pd.read_csv('./data/clustering.csv')
    data = {
        "title_page": title_page,
        "clustering": clustering.iloc[id-1]
    }
    return render_template('clustering/detail.htm', datas=data)


@clustering_ctrl.route('/clustering/store', methods=['post'])
def store():
    print('Request : ', request.form)
    data = pd.read_csv('./data/sebaran-hotspot-agustus.csv')

    _cluster = int(request.form['cluster'])
    _crossover = float(request.form['crossover'])
    _mutation = float(request.form['mutation'])
    _population = int(request.form['population'])
    _iteration = int(request.form['iteration'])

    clustering = Clustering(data=data, cluster=_cluster, crossover=_crossover,
                            mutation=_mutation, population_size=_population, max_iteration=_iteration)
    clustering.process()

    return redirect("/clustering")
