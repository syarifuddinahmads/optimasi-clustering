from flask import Blueprint, redirect, render_template
home_ctrl = Blueprint('home_controller', __name__)
title_page = "Home"


@home_ctrl.route('/')
def index():
    return redirect('data')

@home_ctrl.route('/home')
def home():
    data = {
        "title_page": title_page
    }
    return render_template('home/index.htm', datas=data)
