# load library
import pandas as pd
from services.clustering import Clustering

# load data
data = pd.read_csv('./data/sebaran-hotspot-agustus.csv')

# define variable scenario testing
# clusters = [2, 3, 6, 9, 10]
# crossover_rates = [0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
# mutation_rates = [0.1, 0.2, 0.3]
# population_size = [100, 300, 700, 900, 1000]
# max_iterations = [500, 1000, 1500]

# clusters = [3]
# crossover_rates = [0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
# mutation_rates = [0.1, 0.2, 0.3]
# population_size = [100, 300, 700, 900, 1000]
# max_iterations = [500, 1000, 1500]

# clusters = [3]
# crossover_rates = [0.9]
# mutation_rates = [0.3]
# population_size = [1000]
# max_iterations = [1000,1500]

clusters = [3]
crossover_rates = [ 0.8, 0.9]
mutation_rates = [0.1, 0.2, 0.3]
population_size = [100]
max_iterations = [100]


# loop clusters
for c in clusters:
    # loop population size
    for pop_size in population_size:
        # loop crossovers
        for cr in crossover_rates:
            # loop mutations
            for mr in mutation_rates:
                # loop max iteration
                for max_iter in max_iterations:
                    print(">>> Cluster : ", c, " Population : ", pop_size,
                          " Max Iteration : ", max_iter, " Crossover : ", cr, " Mutation : ", mr)
                    # clustering with kmeans
                    clustering = Clustering(data=data, cluster=c, crossover=cr,
                                            mutation=mr, population_size=pop_size, max_iteration=max_iter, start_date='', end_date='')
                    clustering.process()
