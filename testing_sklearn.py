# load library
import numpy as np
from models.hotspot import hotspot_model
from services.clustering import Clustering
from sklearn.cluster import KMeans
import pandas as pd

# load data
hotspots = hotspot_model.get_hotspots()
data = pd.read_csv('./data/sebaran-hotspot-agustus.csv')

print("Data : \n", data)

# define variable scenario testing
clusters = [2, 3, 4, 5, 6, 7, 8, 9, 10]
crossover_rates = [0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
mutation_rates = [0.1, 0.2, 0.3]
population_size = [100, 300, 500, 700, 900, 1000]
max_iterations = [500, 1000, 1500]


# loop clusters
for c in clusters:
    colors = np.random.rand(c, 3)
    # loop population size
    for pop_size in population_size:
        # loop crossovers
        for cr in crossover_rates:
            # loop mutations
            for mr in mutation_rates:
                # loop max iteration
                for max_iter in max_iterations:
                    print(">>> Cluster : ", c, " Population : ", pop_size,
                          " Max Iteration : ", max_iter, " Crossover : ", cr, " Mutation : ", mr)

                    clustering = Clustering(data=data, cluster=c, crossover=cr,
                                            mutation=mr, population_size=pop_size, max_iteration=max_iter, start_date='', end_date='')
                    clustering.process()
